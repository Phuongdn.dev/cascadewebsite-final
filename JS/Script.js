$('.beers-slide').slick({
        dot: true,
        infinite: true,
        slidesToShow: 2.3,
        slidesToScroll: 3,
        prevArrow: null,
        // nextArrow: null,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 320,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
$('.process-slide').slick({
  dot: true,
  infinite: true,
  slidesToShow: 3.7,
  slidesToScroll: 3,
  prevArrow: null,
  // nextArrow: null,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1.1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 320,
      settings: {
        slidesToShow: 1.1,
        slidesToScroll: 1
      }
    }
  ]
});
